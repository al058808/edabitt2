import java.util.*;
public class NextToLargest {
	public static int secondLargest(int[] num) {
		Arrays.sort(num);
		return num[num.length-2];
	}
}
