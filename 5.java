import java.util.Arrays; 
public class Challenge {
	public static int maxTotal(int[] nums) {
		int total = 0;
		Arrays.sort(nums);
		for(int i=nums.length-1;i>nums.length-6;i--)
			total+=nums[i];
		return total;
	}
}
